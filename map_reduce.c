/*
 ============================================================================
 Name        : map_reduce.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include "mpi.h"

/********************* COMMON VARIABLES ***************************/
int  p;       /* number of processes */
int nr_of_files = 5;
char files[50][80];
char path[] = "input_files";
char *full_path;
char *root_path;

MPI_Status status ;   /* return status for receive */
int tag=0;    /* tag for messages */
int  my_rank; /* rank of process */
int index_of_file;
int nr_of_slave_files;

typedef struct{
	char** terms;
	int *count;
	int nr_of_terms;
}parsed_data;
/******************************************************************/

#define nullptr 0
#define length_max_of_term 150
#define nr_max_of_terms 10000
/******************************************************************/
#define comunicare	0
#define executare	1
#define stop		2
#define request 	3
/******************************************************************/
int* dispatch_tasks()
{
	int *A = (int*)malloc(2*sizeof(int));
	char temp[80];
	full_path = (char*)malloc( strlen(path) + strlen("/_process") +2);
	root_path = (char*)malloc( strlen(path) +2);
	strcpy(root_path,path);

	strcpy(full_path,path);
	strcat(full_path,"/_process");		

	if (my_rank ==0){	// master
		/* create message */

		/******************************************************/
		/************ Organize the work for the slaves ********/
		/******************************************************/
		int nr_max_slaves = p-1;
		int d = 0 , r = 0;
		
		d = nr_of_files / nr_max_slaves;
		r = nr_of_files % nr_max_slaves;

		index_of_file = 0;
		nr_of_slave_files = index_of_file + d + (int)(r>0);
		for(int i=1;i<=nr_max_slaves;i++)
		{
			//A[0] = index_of_file;
			//A[1] = nr_of_slave_files;
			int temp_file_count = 0;
			for(int j=index_of_file; j< index_of_file + nr_of_slave_files;j++)
			{
				temp_file_count = strlen(files[j]) + 1;
				MPI_Send(&temp_file_count,1,MPI_INT,i,0, MPI_COMM_WORLD);
				//printf("Masterul a trimis slaveului %d lungimea cuvantului %d\n",i,temp_file_count);
				MPI_Send(files[j],temp_file_count,MPI_CHAR,i,0, MPI_COMM_WORLD);
				//printf("Masterul a trimis slaveului %d cuvantul %s\n",i,files[j]);
			}
			temp_file_count = 0;
			MPI_Send(&temp_file_count,1,MPI_INT,i,1, MPI_COMM_WORLD);
			//MPI_Send(A, 2, MPI_INT,i, tag, MPI_COMM_WORLD);
			//printf("ROOT> Am trimis %d si %d catre %d.\n",A[0],A[1],i);

			if(r>0) r--;	
			index_of_file = index_of_file + nr_of_slave_files;
			nr_of_slave_files = d + (int)(r>0);

			/******************************************************/
			/************ Create directories for the slaves *******/
			/******************************************************/
			char temp2[80];
			strcpy(temp,full_path);
			sprintf(temp2,"%d",i);
			strcat(temp, temp2);
			sprintf(temp2,"mkdir %s",temp);
			system(temp2);

			strcpy(temp,full_path);
			sprintf(temp2,"%d_reduced",i);
			strcat(temp, temp2);
			sprintf(temp2,"mkdir %s",temp);
			system(temp2);

			/******************************************************/
		}
		/******************************************************/
	}
	else{	//slaves	
		//MPI_Recv(A, 2, MPI_INT, 0, tag,MPI_COMM_WORLD, &status);
		//printf("P%d> Am primit %d si %d.\n",my_rank,A[0],A[1]);
		//index_of_file = A[0];
		//nr_of_slave_files = A[1];
		int temp_file_count = 0;
		int i=0;
		MPI_Recv(&temp_file_count, 1, MPI_INT, 0, MPI_ANY_TAG,MPI_COMM_WORLD, &status);
		//printf("Slaveul %d a primit lungimea cuvantului %d \n",my_rank, temp_file_count);
		while(temp_file_count != 0)
		{
			MPI_Recv(files[i], temp_file_count, MPI_CHAR, 0, MPI_ANY_TAG,MPI_COMM_WORLD, &status);
			//printf("Slaveul %d a primit numele fisierului %s\n",my_rank, files[i]);
			i++;
			MPI_Recv(&temp_file_count, 1, MPI_INT, 0, MPI_ANY_TAG,MPI_COMM_WORLD, &status);
			//printf("Slaveul %d a primit lungimea cuvantului %d \n",my_rank, temp_file_count);
			if (status.MPI_TAG == 1)
			{
				break;
			}
		}
		index_of_file = 0;
		nr_of_slave_files = i;

		////// ASTA E OK NU STERGE, se completeaza path`ul 
				////////////   cu numele folderelor pt fiecare slave
		sprintf(temp,"%d",my_rank);
		strcat(full_path, temp);
		//////////////////////////////////////

		// 
	}
	MPI_Barrier(MPI_COMM_WORLD);
	return A;
}

/***************************************************
****************** MAPPING PHASE *******************
***************************************************/
void* parsing(FILE* file)
{
	char line[length_max_of_term];
	char terms[nr_max_of_terms][length_max_of_term];
	int terms_count[nr_max_of_terms];
	int nr_of_terms = 0;
	int i;
	for(i=0;i<nr_max_of_terms;i++)
	{
		strcpy(terms[i],"\0");
		terms_count[i] = 0;
	}
	fscanf(file,"%49[^a-zA-Z]",line);
	while((fscanf(file,"%49[a-zA-Z]",line)) == 1)
	{
		for(i=0;i<nr_of_terms;i++)
		{
			if(strstr(line,terms[i]) == line && strlen(line) == strlen(terms[i]))
			{
				break;
			}
		}
		
		// if new word is found then it must be saved
		if(i >= nr_of_terms)
		{
			strcpy(terms[i],line);
			//printf("%s\n",terms[nr_of_terms]);
			nr_of_terms ++ ;
		}
		// this needs to be increased:
		//	if new word then it must be 1
		//	else it means we found again a word we already had so 
		//			so it needs to be increased
		terms_count[i] ++;
		fscanf(file,"%49[^a-zA-Z]",line);

	}

	parsed_data* ret = (parsed_data*)malloc(sizeof(parsed_data));
	
	ret->terms = (char**)malloc(nr_of_terms*sizeof(char*));
	ret->count = (int*)malloc(nr_of_terms*sizeof(int));
	for(i=0;i<nr_of_terms;i++)
	{
		ret->terms[i] = (char*)malloc(strlen(terms[i])*sizeof(char) + 1);
		strcpy(ret->terms[i],terms[i]);
		ret->count[i] = terms_count[i];
	}
	ret->nr_of_terms = nr_of_terms;
	return (void*)ret;
}

void generate_maped_data(parsed_data *parsed_file,char* file_parsed)
{
	FILE *temp_pfile;
	char temp[80];

	//printf("%d parsed files nr of terms %d\n",my_rank,parsed_file->nr_of_terms);
	for(int i=0;i<parsed_file->nr_of_terms;i++)
	{
		sprintf(temp,"./%s/%s",full_path,parsed_file->terms[i]);
		temp_pfile = fopen(temp,"a");
		fprintf(temp_pfile,"%s %d\n",file_parsed,parsed_file->count[i]);

		fclose(temp_pfile);
	}

}

parsed_data** mapping()
{
	if(my_rank != 0)
	{
		parsed_data** parsed_file = (parsed_data**)malloc(nr_of_slave_files * sizeof(parsed_data*));
		FILE** slave_files = (FILE**)malloc(nr_of_slave_files * sizeof(FILE*));

		for(int i=index_of_file;i<index_of_file+nr_of_slave_files;i++)
		{
			int temp_index = i - index_of_file;
			slave_files[temp_index] = fopen(files[i],"r");
			if(slave_files[temp_index] == nullptr)
			{
				printf("! Procesorul %d nu poate deschide fisierul %s!\n",my_rank,files[i]);
			}
		}

		/************ HERE SHOULD BE THE PARSING OF THE FILES *****************/
		for(int i=0;i<nr_of_slave_files;i++)
		{
			parsed_file[i] = parsing(slave_files[i]);
		}
		/**********************************************************************/

		/****************** Generate the maped files***********************/
		for(int i=0;i<nr_of_slave_files;i++)
		{
			generate_maped_data(parsed_file[i],files[i+index_of_file]);
		}
		/**************************************************/



		// close the files which i had read
		for(int i=0;i<nr_of_slave_files;i++)
		{
			fclose(slave_files[i]);
		}
		return parsed_file;
	}
	else
	{
		return (parsed_data**)0;
	}

}
/**************************************************/


/***************************************************
****************** SORT TERMS **********************
***************************************************/
parsed_data* reduce_slave_dictionary(parsed_data ** dict)
{
	char terms[nr_max_of_terms][length_max_of_term];
	int nr_of_terms = 0;
	int i,j,k;
	for(i=0;i<nr_of_slave_files;i++)
	{
		for(j= dict[i]->nr_of_terms-1;j>=0;j--)
		{
			for(k=nr_of_terms;k>=0;k--)
			{
				if(strstr(terms[k],dict[i]->terms[j]) == *terms+k && strlen(terms[k]) == strlen(dict[i]->terms[j]))
				{
					break;
				}
			}
			if(k<0)
			{
				strcpy(terms[nr_of_terms],dict[i]->terms[j]);
				nr_of_terms ++;
			}
		}
	}
	for(i=0;i<nr_of_slave_files;i++)
	{
		for(j=dict[i]->nr_of_terms-1;j>=0;j--)
		{
			free(dict[i]->terms[j]);
		}
		free(dict[i]->count);
		free(dict[i]);
	}
	free(dict);
	parsed_data *ret = (parsed_data*)malloc(sizeof(parsed_data));
	ret->terms = (char**)malloc(sizeof(char*)*nr_of_terms);
	ret->nr_of_terms = nr_of_terms;
	int temp_length;
	for(int i=0;i<nr_of_terms;i++)
	{
		temp_length = strlen(terms[i]);
		ret->terms[i] = (char*)malloc(temp_length * sizeof(char) + 1);
		strcpy(ret->terms[i], terms[i]);
	}
	return ret;
}


void reducing_files(char term_received[])
{
	char path_to_term_received[200];
	char temp[100];
	char lines_from_file[nr_max_of_terms][length_max_of_term];
	int number_of_appearences[nr_max_of_terms];
	char line_read_temp[length_max_of_term];
	int number_of_appearences_temp;
	int index_of_line = 0;
	FILE* pFile;
	int j;
//	printf("Process %d termen %s\n",my_rank,term_received);
	for(int i=1;i<p;i++)
	{
		strcpy(path_to_term_received,root_path);
		strcat(path_to_term_received,"/_process");
		sprintf(temp,"%d/",i);
		strcat(path_to_term_received, temp);
		strcat(path_to_term_received, term_received);
		pFile = fopen(path_to_term_received,"r");
		//printf("%d path`ul este %s\n",my_rank,path_to_term_received);

		if(pFile != NULL)
		{
			while(fscanf(pFile,"%s",line_read_temp) != EOF)
			{
				fscanf(pFile,"%d",&number_of_appearences_temp);
				//printf("Procesor %d >> <%s> <%d>\n",my_rank,line_read_temp,number_of_appearences_temp);
				for(j=0;j<index_of_line;j++)
				{
					if(strstr(line_read_temp,lines_from_file[j]) == line_read_temp && strlen(line_read_temp) == strlen(lines_from_file[j]))
					{
						break;
					}
				}
				if(j>=index_of_line)
				{
					strcpy(lines_from_file[index_of_line],line_read_temp);
					number_of_appearences[index_of_line] = number_of_appearences_temp;
					//printf("%d Adaug termenul %s\n",my_rank,lines_from_file[index_of_line]);
					index_of_line++;
				}
				else
				{
					number_of_appearences[j] += number_of_appearences_temp;
					//printf("%d Updatez termenul %s\n",my_rank,lines_from_file[j]);
				}
			}
			fclose(pFile);
			pFile = NULL;

		}
		
	}
	char path_to_reduced[200];
	strcpy(path_to_reduced,root_path);
	strcat(path_to_reduced,"/_process");
	sprintf(temp,"%d_reduced/",my_rank);
	strcat(path_to_reduced, temp);
	strcat(path_to_reduced, term_received);

	//printf("<%d> %s\n",my_rank,path_to_reduced);
	pFile = fopen(path_to_reduced, "w");
	if(pFile != NULL)
	{
		for(int i=0;i<index_of_line;i++)
		{
			fprintf(pFile,"%s %d\n",lines_from_file[i] ,number_of_appearences[i]);
		}
	}

}

void reducing(parsed_data* data)
{
	if(my_rank != 0)
	{
		int command=0;
		int command_info = request;
		int length_of_term;
		int length_of_recv_term;
		char term_received[length_max_of_term];

		MPI_Send(&command_info,1,MPI_INT,0,request,MPI_COMM_WORLD);
		//printf(">>>> Procesorul %d face request!\n",my_rank);
		MPI_Recv(&length_of_recv_term,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
		command = status.MPI_TAG;
		//printf("**** Procesorul %d a primit comanda %d.\n",my_rank,command);

		while(command != stop)
		{
			if(command == executare)
			{
				MPI_Recv(term_received,length_of_recv_term,MPI_CHAR,0,executare,MPI_COMM_WORLD,&status);
				//printf("<%d> AM PRIMIT COMANDA %s  - %d!\n",my_rank,term_received,length_of_recv_term);
				reducing_files(term_received);
			}
			else if (command == comunicare)
			{
				int nr_of_terms = data->nr_of_terms;
				int length_of_term;
				for(int i=0;i<nr_of_terms;i++)
				{
					length_of_term = strlen(data->terms[i]) + 1;
					//printf("((())))Procesorul %d trimite termenul %s\n",my_rank,data->terms[i]);
					MPI_Send(&length_of_term,1,MPI_INT,0,comunicare,MPI_COMM_WORLD);
					MPI_Send(data->terms[i],length_of_term,MPI_CHAR,0,comunicare,MPI_COMM_WORLD);
				}
				length_of_term = 0;
				MPI_Send(&length_of_term,1,MPI_INT,0,comunicare,MPI_COMM_WORLD);
			}

			// REQUEST A NEW COMMAND
			MPI_Send(&command_info,1,MPI_INT,0,request,MPI_COMM_WORLD);
			//printf("> Procesorul %d face request!\n",my_rank);
			//printf(" << >> Procesorul %d astapta o comanda!!!\n",my_rank);
			MPI_Recv(&length_of_recv_term,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			command = status.MPI_TAG;
			//printf("* Procesorul %d a primit comanda %d\n",my_rank,command);	
		}
		
	}
	else
	{
		int char_temp =stop;
		char terms[nr_max_of_terms][length_max_of_term];
		char received_word[length_max_of_term];
		int nr_of_terms = 0;
		int index_of_terms = 0;
		int length_of_term = 0;
		int slaves_waiting[p];
		int j;

		/******* to comunicate ************/
		int source;
		/**********************************/
		for(int i=0;i<p;i++) slaves_waiting[i] = 0;

		for(int i=1;i<p;i++)
		{
			/******* INFORMEZ UN SLAVE CA SA IMI TRIMITA DICTIONARUL SAU ***********/
			if(slaves_waiting[i] == 0)
			{
				MPI_Recv(&char_temp,1,MPI_INT,i,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
				//printf("ROOT: Procesorul %d face requestul %d\n",status.MPI_SOURCE,status.MPI_TAG);
			}
			char_temp = comunicare;
			MPI_Send(&char_temp,1,MPI_INT,i,comunicare,MPI_COMM_WORLD);
			/***********************************************************************/

			length_of_term = 1;
			while(length_of_term != 0)
			{
				MPI_Recv(&char_temp, 1, MPI_INT,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
				//printf("Am primit un request cu tagul %d (%d) > [%d]\n",status.MPI_TAG,status.MPI_SOURCE,char_temp);
				if(status.MPI_TAG ==comunicare)
				{
					length_of_term = char_temp;
					if(length_of_term == 0 )
					{
						break;
					}
					else
					{
						MPI_Recv(received_word, length_of_term, MPI_CHAR,i,comunicare,MPI_COMM_WORLD,&status);
						//printf("ROOT: Am primit termenul %s de la %d\n",received_word,status.MPI_SOURCE);
						//// VERIFIC DACA NU CUNOSC DEJA ACEST TERMEN
						for(j=0;j<nr_of_terms;j++)
						{
							if(strstr(received_word,terms[j]) == received_word && strlen(received_word) == strlen(terms[j]))
							{
								printf("Am comparat cu succes %s cu %s\n",received_word,terms[j]);
								break;
							}
						}
						if(j>=nr_of_terms)
						{
							strcpy(terms[nr_of_terms],received_word);
							//printf("ROOT: Am adaugat termenul %s in dictionar.\n",terms[nr_of_terms]);
							nr_of_terms ++;
						}
					}
				}
				else
				{
					source = status.MPI_SOURCE;
					if(index_of_terms < nr_of_terms)
					{
						//printf("ROOT: Am trimis termenul %s la procesorul %d\n",terms[index_of_terms],source);
						char_temp = strlen(terms[index_of_terms]) + 1;
						MPI_Send(&char_temp,1,MPI_INT,source,executare,MPI_COMM_WORLD);
						MPI_Send(terms[index_of_terms],char_temp,MPI_CHAR,source,executare,MPI_COMM_WORLD);
						index_of_terms ++ ;
					}
					else
					{
						//printf("ROOT > Am pus sursa %d in coada de asteptare.\n",source);
						slaves_waiting[source] = 1;
					}
				}

				/******* DACA AM SLAVE CARE ASTEAPTA ******/
				for(j=1;j<p;j++)
				{
					if(slaves_waiting[j] == 1)
					{
						if(index_of_terms < nr_of_terms)
						{
							char_temp = strlen(terms[index_of_terms]) + 1;
							MPI_Send(&char_temp,1,MPI_INT,j,executare,MPI_COMM_WORLD);
							MPI_Send(terms[index_of_terms],char_temp,MPI_CHAR,j,executare,MPI_COMM_WORLD);
							index_of_terms ++;
							slaves_waiting[j] = 0;
							//printf("Am trimis termenul %s si am scos din coada procesorul %d\n",terms[index_of_terms-1],j);
						}
					}
				}
				/*****************************************************/

			}
			for(j=1;j<p;j++)
			{
				if(slaves_waiting[j] == 1)
				{
					if(index_of_terms < nr_of_terms)
					{
						char_temp = strlen(terms[index_of_terms]) + 1;
						MPI_Send(&char_temp,1,MPI_INT,j,executare,MPI_COMM_WORLD);
						MPI_Send(terms[index_of_terms],strlen(terms[index_of_terms]),MPI_CHAR,j,executare,MPI_COMM_WORLD);
						//printf("ROOT >> Am scos sursa %d din asteptare.\n",j);
						index_of_terms ++;
						slaves_waiting[j] = 0;
					}
				}
			}
		}

		for(;index_of_terms <nr_of_terms;index_of_terms++ )
		{
			MPI_Recv(&char_temp, 1, MPI_INT,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			//printf("Am primit un request cu tagul %d (%d) > [%d]\n",status.MPI_TAG,status.MPI_SOURCE,char_temp);
			char_temp = strlen(terms[index_of_terms]) + 1;
			MPI_Send(&char_temp,1,MPI_INT,status.MPI_SOURCE,executare,MPI_COMM_WORLD);
			MPI_Send(terms[index_of_terms],char_temp,MPI_CHAR,status.MPI_SOURCE,executare,MPI_COMM_WORLD);
		}

		for(int i=1;i<p;i++)
		{
				char_temp = stop;
				MPI_Send(&char_temp,1,MPI_INT,i,stop,MPI_COMM_WORLD);
				printf("ROOT: Am trimis comanda de STOP procesorului %d\n",i);
		}
	}
}

/**************************************************/

int main(int argc, char* argv[]){	

	/***************************************************
	************* SETUP MPI ENVIRONENT *****************
	***************************************************/
	/* start up MPI */
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	/****************************************************/
	
	////////// create the correct files path
	char temp[80];
	if (my_rank == 0)
	{
		printf("Introduceti numele folderului care sa fie procesat: \n");
		scanf("%s",path);
		system("clear");
		strcpy(temp,"./");
		strcat(temp,path);
		strcpy(path, temp);
		int temp2 = strlen(path)+1;
		for(int i=1; i<p;i++)
		{
			MPI_Send(&temp2,1,MPI_INT,i,0, MPI_COMM_WORLD);
			MPI_Send(path,temp2,MPI_CHAR,i,0, MPI_COMM_WORLD);
		}
		DIR *dir;
		struct dirent *ent;
		int i=0;
		if ((dir = opendir (temp)) != NULL) {
		  /* print all the files and directories within directory */
		  while ((ent = readdir (dir)) != NULL) {
		  	if(!(strcmp(ent->d_name,".")==0 || strcmp(ent->d_name,"..")==0)){
			    //printf ("%s\n", ent->d_name);
				strcpy(temp,path);
			    strcat(temp,"/");
				strcat(temp,ent->d_name);
				strcpy(files[i],temp);
				//printf("%s \n",files[i]);
				i++;
			}
		}
		  closedir (dir);
		} 
		else {
		  /* could not open directory */
		  perror ("");
		  return EXIT_FAILURE;
		}

	}
	else
	{
		int temp = 0;
		MPI_Recv(&temp,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
		MPI_Recv(&path,temp,MPI_CHAR,0,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
		// the slaves do not concern with this matter

	}
	///////////////////////////////////////

	/*****************************************************
	/***************** DISPATCH TASKS ********************
	*****************************************************/
	dispatch_tasks();
	/*****************************************************/
	MPI_Barrier(MPI_COMM_WORLD);
	parsed_data** slave_dict = mapping();
	MPI_Barrier(MPI_COMM_WORLD);
	parsed_data* slave_dict_reduct = 0;
	if(slave_dict != 0)
	{
		slave_dict_reduct = reduce_slave_dictionary(slave_dict);
	}
	reducing(slave_dict_reduct);
	
	/*****************************************************
	/********** MASTER NEEDS TO COUNT ********************
	*****************************************************/





	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
